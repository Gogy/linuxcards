import _ from 'lodash';
import { EventEmitter } from 'events';
import Dispatcher from '../dispatcher';

import * as events from '../events/card';

const CHANGE = 'change';

class CardStore extends EventEmitter {

	constructor () {
		super();
		this.cards = [];
	}

	getState () {
		return this.cards;
	}

	getCard (id) {
		return _.find(this.cards, {id: id});
	}

	deleteCard (atId) {
		this.cards = _.reject(this.cards, {id: atId});
	}

	setFavorite (atId) {
		this.cards = _.map(this.cards, card => {
			if (card.id === atId) {
				return {
					...card,
					favorite: !card.favorite
				};
			}
			return card;
		})
	}

	handleActions (action) {
		switch(action.type) {
			case events.FETCH_CARDS_DATA:
				this.cards = action.data;
				this.emit(CHANGE);
			break;

			case events.DELETE_CARD:
				this.deleteCard(action.id);
				this.emit(CHANGE);
				break;
			case events.SET_FAVORITE:
				this.setFavorite(action.id);
				this.emit(CHANGE);
				break;
		}
	}
}

const cardStore = new CardStore;

Dispatcher.register(cardStore.handleActions.bind(cardStore));

export default cardStore;
