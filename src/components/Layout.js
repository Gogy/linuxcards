import React from 'react';
import Cards from './Cards';

export default class Layout extends React.Component {

	render () {
		return (
			<div>
				<div className="container-fluid">
					<div className="row">
						<Cards />
					</div>
				</div>
			</div>
		)
	}
}
