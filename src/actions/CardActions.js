import superagent from 'superagent';
import Dispatcher from '../dispatcher'
import * as events from '../events/card';

const API = 'https://api.myjson.com/bins/1euzq';

export function deleteCard (id) {
	Dispatcher.dispatch({
		type: events.DELETE_CARD,
		id
	});
}

export function setFavorite (id) {
	Dispatcher.dispatch({
		type: events.SET_FAVORITE,
		id
	});
}

export function fetchCards () {
	superagent.get(API).then(resp => {
		Dispatcher.dispatch({
			type: events.FETCH_CARDS_DATA,
			data: resp.body
		});
	});
}
