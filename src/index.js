import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';

import Layout from './components/Layout';
import CardDetails from './components/CardDetails';

ReactDOM.render(
	<Router history={browserHistory}>
		<Route path="/" component={Layout} />
		<Route path="/card/:id" component={CardDetails} />
	</Router>,
	document.getElementById('root')
);
