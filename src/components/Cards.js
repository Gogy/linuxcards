import _ from 'lodash';
import React from 'react';
import Card from './Card';
import CardStore from '../stores/CardStore';
import {fetchCards, deleteCard, setFavorite} from '../actions/CardActions';

export default class Cards extends React.Component {

	constructor () {
		super();
		this.state = this.getState();
		this.setToState = this.setToState.bind(this);
	}

	componentWillMount () {
		CardStore.on('change', this.setToState);
	}

	componentWillUnmount () {
		CardStore.removeListener('change', this.setToState)
	}

	componentDidMount () {
		//Fetch the cards only when there are none inside as react router remounts the components.
		if (this.state.cards.length === 0) {
			fetchCards();
		}
	}

	setToState () {
		this.setState(this.getState())
	}

	getState () {
		return {
			cards: CardStore.getState()
		};
	}

	renderCard (card) {
		return (
			<Card
				header={card.header}
				description={card.description}
				favorite={card.favorite}
				image={card.background}
				key={card.id}
				id={card.id}
				onDeleteHandler={() => deleteCard(card.id)}
				onFavoriteHandler={() => setFavorite(card.id)}
			/>
		)
	}

	render () {
		return (
			<div>
				{_.map(this.state.cards, this.renderCard)}
			</div>
		)
	}
}
