import React, {PropTypes} from 'react';
import {Link} from 'react-router';

const FAVORITE_ICON = 'http://i.imgur.com/F3pjPwb.png';

export default class Card extends React.Component {

	static propTypes = {
		id: PropTypes.number,
		header: PropTypes.string,
		description: PropTypes.string,
		image: PropTypes.string,
		favorite: PropTypes.bool,
		onDeleteHandler: PropTypes.func,
		onFavoriteHandler: PropTypes.func
	}

	constructor () {
		super()
		this.state = {overlay: 'hideOverlay'}

		this.mouseOver = this.mouseOver.bind(this);
		this.mouseOut = this.mouseOut.bind(this);
	}

	favoriteButton (favorited) {
		return (favorited) ? 'Un-favorite' : 'Favorite';
	}

	mouseOver () {
		this.setState({overlay: 'showOverlay'});
	}

	mouseOut () {
		this.setState({overlay: 'hideOverlay'});
	}

	setBackground(image) {
		return {
			backgroundImage: "url("+image+")"
		};
	}

	render () {

		const {id, header, description, favorite, image, onDeleteHandler, onFavoriteHandler} = this.props;
		const {overlay} = this.state;

		return (
			<div className='col-xs12 col-sm-6 col-md-3 col-lg-3 col' style={this.setBackground(image)}>
				<div className='box' onMouseOver={this.mouseOver} onMouseOut={this.mouseOut}>
					<h2>{header}</h2>
					<p>{description}</p>

					{(favorite) && <img src={FAVORITE_ICON} className='favorite_icon' />}

					<div className={overlay}>
						<button className='btn btn-default btn-xs' onClick={onDeleteHandler}>
							<span className='glyphicon glyphicon-remove'></span> Delete
						</button>
						<button className='btn btn-default btn-xs' onClick={onFavoriteHandler}>
							<span className='glyphicon glyphicon-star'></span> {this.favoriteButton(favorite)}
						</button>
						<Link to={`/card/${id}`} className='btn btn-default btn-xs'>
							<span className='glyphicon glyphicon-eye-open'></span> Open
						</Link>
					</div>

				</div>
			</div>
		)
	}
}
