import React, {PropTypes} from 'react';
import CardStore from '../stores/CardStore';
import { Link } from 'react-router';

class CardDetails extends React.Component {

	static propTypes = {
		params: PropTypes.object
	}

	constructor (props) {
		super(props);

		this.getState = this.getState.bind(this);
		this.state = this.getState();
		this.setToState = this.setToState.bind(this);
	}

	componentWillMount () {
		CardStore.on('change', this.setToState );
	}

	componentWillUnmount () {
		CardStore.removeListener('change', this.setToState)
	}

	getState () {
		return {
			card: CardStore.getCard(parseInt(this.props.params.id))
		};
	}

	setToState () {
		this.setState(this.getState())
	}

	render () {
		return (
			<div>
				<div className="container">
					<div className="row">
						<div className="page-header">
  							<h1>{this.state.card.header}</h1>
						</div>
						<div className="well">
						<p>{this.state.card.text}</p>
						</div>
						<p><a href={this.state.card.url}>Visit official website</a></p>
						<Link to="/" className="btn btn-primary test">Back</Link>
					</div>
				</div>
			</div>
			)
	}
}

CardDetails.contextTypes = {
	router: PropTypes.object
};

export default CardDetails;
